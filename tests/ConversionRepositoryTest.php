<?php
namespace ZinotechMum\Tests;

use ZinotechMum\ConversionRepository;
use PHPUnit\Framework\TestCase;
use ZinotechMum\Exception\ConvertorException;
use ZinotechMum\Exception\ConvertorInvalidUnitException;
use ZinotechMum\Exception\FileNotFoundException;

/**
 * Class ConversionRepositoryTest
 * @package ZinotechMum\Tests
 */
class ConversionRepositoryTest extends TestCase
{
    /**
     * @throws ConvertorException
     * @throws ConvertorInvalidUnitException
     * @throws FileNotFoundException
     */
    public function test_km_to_base_conversion(): void
    {
        $repository = ConversionRepository::fromFile(__DIR__.'/../src/Config/Units.php');

        $this->assertInstanceOf(ConversionRepository::class, $repository);

        $kilometers = 8.5;
        $meters = $repository->getConversion('km')->convertToBase($kilometers);
        $this->assertEquals(8500, $meters);
    }
}