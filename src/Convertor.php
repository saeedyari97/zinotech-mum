<?php

namespace ZinotechMum;

use ZinotechMum\Exception\ConvertorDifferentTypeException;
use ZinotechMum\Exception\ConvertorException;
use ZinotechMum\Exception\ConvertorInvalidUnitException;
use ZinotechMum\Exception\FileNotFoundException;

/**
 * Class Convertor
 * @package ZinotechMum
 */
class Convertor
{
    /** @var ?float */
    private $value;

    /** @var ?string */
    private $baseUnit;

    /** @var ConversionRepository */
    private $units;

    /**
     * Convertor constructor.
     * @param float|null $value
     * @param string|null $unit
     * @param string|null $unitFile
     * @throws ConvertorException
     * @throws ConvertorInvalidUnitException
     * @throws FileNotFoundException
     */
    public function __construct(?float $value = null, ?string $unit = null, ?string $unitFile = null)
    {
        $this->loadUnits($unitFile);

        if (! is_null($value)) {
            $this->from($value, $unit);
        }
    }

    /**
     * Allow switching between different unit definition files. Defaults to src/Config/Units.php
     * @param ?string $path Load your own units file if you want.
     * @throws FileNotFoundException|ConvertorException
     */
    private function loadUnits(?string $path = null): void
    {
        if (! $path) {
            $path =  __DIR__ . '/Config/Units.php';
        }
        $this->units = ConversionRepository::fromFile($path);
    }

    /**
     * Set from conversion value / unit
     *
     * @param float $value -  a numeric value to base conversions on
     * @param string|null $unit (optional) - the unit symbol for the start value
     * @return Convertor
     * @throws ConvertorException - general errors
     * @throws ConvertorInvalidUnitException - specific invalid unit exception
     */
    public function from(float $value, ?string $unit = null): Convertor
    {
        if (! $unit) {
            $this->value = $value;
            return $this;
        }

        if (! $this->units->unitExists($unit)) {
            throw new ConvertorInvalidUnitException("Conversion from Unit u=$unit not possible - unit does not exist.");
        }

        $conversion     = $this->units->getConversion($unit);
        $this->baseUnit = $conversion->getBaseUnit();
        $this->value    = $conversion->convertToBase($value);
        return $this;
    }

    /**
     * Convert from value to new unit
     *
     * @param string $unit -  the unit symbol (or array of symbols) for the conversion unit
     * @param    ?int $decimals (optional, default-null) - the decimal precision of the conversion result
     * @param boolean $round (optional, default-true) - round or floor the conversion result
     * @return   float
     * @throws ConvertorDifferentTypeException
     * @throws ConvertorException
     * @throws ConvertorInvalidUnitException
     */
    public function to(string $unit, ?int $decimals = null, bool $round = true): float
    {
        if (is_null($this->value)) {
            throw new ConvertorException("From Value Not Set.");
        }

        if (! $this->units->unitExists($unit)) {
            throw new ConvertorInvalidUnitException("Conversion from Unit u=$unit not possible - unit does not exist.");
        }

        $conversion = $this->units->getConversion($unit);

        if (! $this->baseUnit) {
            $this->baseUnit = $conversion->getBaseUnit();
        }

        if ($conversion->getBaseUnit() !== $this->baseUnit) {
            throw new ConvertorDifferentTypeException("Cannot Convert Between Units of Different Types");
        }

        $result = $conversion->convertFromBase($this->value);

        if (! is_null($decimals)) {
            return $this->round($result, $decimals, $round);
        }
        return $result;
    }

    /**
     * @param string $unit
     * @param string $base
     * @param float|Callable $conversion
     * @return bool
     * @throws ConvertorException
     * @throws ConvertorInvalidUnitException
     */
    public function addUnit(string $unit, string $base, $conversion): bool
    {
        $conversion = new ConversionDefinition($unit, $base, $conversion);
        $this->units->addConversion($conversion);
        return true;
    }

    /**
     * @param string $unit
     * @return bool
     * @throws ConvertorInvalidUnitException
     */
    public function removeUnit(string $unit): bool
    {
        $this->units->removeConversion($unit);
        return true;
    }

    /**
     * List all available conversion units for given unit.
     * @param string $unit
     * @return string[]
     * @throws ConvertorInvalidUnitException
     */
    public function getUnits(string $unit): array
    {
        return $this->units->getAvailableConversions($unit);
    }

    /**
     * @param float $value
     * @param int $decimals
     * @param bool $round
     * @return float
     */
    private function round(float $value, int $decimals, bool $round): float
    {
        $mode = $round ? PHP_ROUND_HALF_UP : PHP_ROUND_HALF_DOWN;
        return round($value, $decimals, $mode);
    }
}
