<?php

use ZinotechMum\Convertor;

require '../../vendor/autoload.php';

// Define Unit

try {
    //add the kilometre unit
    $precisionConvertor = new Convertor();

    //add Fahrenheit to the temperature category, this has a complex conversion calculation so uses a callback
    $fahrenheitConversion = function($val, $tofrom){
        //$val - value to convert
        //$tofrom - wheather it is being converted to or from the base unit (true - to base unit, false - from base unit to current unit)

        return $tofrom ? ($val * 9/5 - 459.67) : (($val + 459.67) * 5/9);
    };

    $precisionConvertor->addUnit("f", "k", $fahrenheitConversion); //add the Fahrenheit unit to the temperature conversion base unit "k", ;

    $precisionConvertor->from(2, 'f');
    echo $precisionConvertor->to('c');
    echo "<br />";
    //create a new base unit for a sound category, with a base unit of dB
} catch (Exception $exception){
    die($exception->getMessage());
}



// Convert Unit

try {

    $demoConvertor = new Convertor(10, "cm");
    echo $demoConvertor->to("mm"); //convert millimeter to feet
} catch (Exception $exception){
    die($exception->getMessage());
}