<?php
namespace ZinotechMum\Exception;

use Exception;
use Throwable;

/**
 * Class ConvertorDifferentTypeException
 * @package ZinotechMum\Exception
 */
class ConvertorDifferentTypeException extends BaseException
{
    /**
     * ConvertorDifferentTypeException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}