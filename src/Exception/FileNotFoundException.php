<?php
namespace ZinotechMum\Exception;

use Exception;
use Throwable;

/**
 * Class FileNotFoundException
 * @package ZinotechMum\Exception
 */
class FileNotFoundException extends BaseException
{
    /**
     * FileNotFoundException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}