<?php
namespace ZinotechMum\Exception;

use Exception;
use Throwable;

/**
 * Class ConvertorInvalidUnitException
 * @package ZinotechMum\Exception
 */
class ConvertorInvalidUnitException extends BaseException
{
    /**
     * ConvertorInvalidUnitException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}