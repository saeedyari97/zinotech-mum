FROM php:7.4

WORKDIR /var/www

COPY . /var/www

RUN apt-get update && apt-get install -y zip unzip

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

RUN composer update

RUN composer start

EXPOSE 8000