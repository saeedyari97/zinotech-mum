# Zinotech Measurement Units Managements Subsystem
***
This is a mini project in the process of Measurement Units Managements subsystem of Zinotech company to check my programming and problem-solving skills.

## Notice
***
I config this project to run with php built-in webserver, so you can not fine any nginx or other third-party services as webserver to parse php scripts.
Also, I implement custom `composer start` shell command to avoid of typing long commands.

## Requirements
***
+ OS (Linux - Windows - MacOS)
+ Git
+ PHP ^7.4
+ composer

**I send a copy of source with mail attachment, also you can clone code from Bitbucket**

Then clone code from `git clone https://saeedyari97@bitbucket.org/saeedyari97/zinotech-mum.git`

## Installation and Run Project
***
You must run following command:
+ `composer update`
+ `composer start` this command will run project on http://localhost:8000

### Test
***
Run `.\vendor\bin\phpunit` on root of project folder.

### Samples
***
some required sample are implement in `src/public/index.php` path
  
### License
***
The zinotech-mum project is open-sourced software licensed under the [MIT License](./LICENSE.txt)